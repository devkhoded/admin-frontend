import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.onChangeImgurl = this.onChangeImgurl.bind(this);
    this.onChangeProductName = this.onChangeProductName.bind(this);
    this.onChangePrice = this.onChangePrice.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      img_url: '',
      product_name: '',
      price:''
    }
  }

  componentDidMount() {
      axios.get('https://sails-backend11.herokuapp.com/products/'+this.props.match.params.id)
          .then(response => {
              this.setState({ 
                img_url: response.data.url, 
                product_name: response.data.name,
                price: response.data.price });
          })
          .catch(function (error) {
              console.log(error);
          })
    }

  onChangeImgurl(e) {
    this.setState({
      img_url: e.target.value
    });
  }
  onChangeProductName(e) {
    this.setState({
      product_name: e.target.value
    })  
  }
  onChangePrice(e) {
    this.setState({
      price: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      name: this.state.product_name,
      url: this.state.img_url,
      price: this.state.price
    };
    axios.put('https://sails-backend11.herokuapp.com/products/'+this.props.match.params.id, obj)
        .then(res => console.log(res.data));
    alert("Product Updated");
    this.props.history.push('/index');
  }
 
  render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3 align="center">Update Product</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Product Name:  </label>
                    <input 
                      type="text" 
                      className="form-control" 
                      value={this.state.product_name}
                      onChange={this.onChangeProductName}
                      />
                </div>
                <div className="form-group">
                    <label>Product Image Url: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.img_url}
                      onChange={this.onChangeImgurl}
                      />
                </div>
                <div className="form-group">
                    <label>Price: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.price}
                      onChange={this.onChangePrice}
                      />
                </div>
                <div className="form-group">
                    <input type="submit" 
                      value="Update Product" 
                      className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}