import React, { Component } from 'react';
import axios from 'axios';

export default class Create extends Component {
  constructor(props) {
  super(props);
  this.onChangeImgurl = this.onChangeImgurl.bind(this);
  this.onChangeProductName = this.onChangeProductName.bind(this);
  this.onChangePrice = this.onChangePrice.bind(this);
  this.onSubmit = this.onSubmit.bind(this);

  this.state = {
    img_url: '',
    product_name: '',
    price:''
  }
}

onChangeImgurl(e) {
  this.setState({
    img_url: e.target.value
  });
}
onChangeProductName(e) {
  this.setState({
    product_name: e.target.value
  })  
}
onChangePrice(e) {
  this.setState({
    price: e.target.value
  })
}

onSubmit(e) {
  e.preventDefault();
  const obj = {
    url: this.state.img_url,
    name: this.state.product_name,
    price: this.state.price
  };
  axios.post('https://sails-backend11.herokuapp.com/products', obj)
      .then(res => console.log(res.data));
  
  this.setState({
    img_url: '',
    product_name: '',
    price: ''
  })
alert("Product Created");
}
render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3 align="center">Add New Product</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Product Name:  </label>
                    <input 
                      type="text" 
                      className="form-control" 
                      value={this.state.product_name}
                      onChange={this.onChangeProductName}
                      />
                </div>
                <div className="form-group">
                    <label>Product Image Url: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.img_url}
                      onChange={this.onChangeImgurl}
                      />
                </div>
                <div className="form-group">
                    <label>Price: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.price}
                      onChange={this.onChangePrice}
                      />
                </div>
                <div className="form-group">
                    <input type="submit" 
                      value="Add Product" 
                      className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}