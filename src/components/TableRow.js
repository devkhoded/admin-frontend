import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class TableRow extends Component {

  constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }
    delete() {
        axios.delete('https://sails-backend11.herokuapp.com/products/', {params: {id: this.props.obj.id}})
            .then(console.lo('Deleted'))
            .catch(err => console.log(err))
            alert("Product Deleted");
    }
  render() {
    return (
        <tr>
          <td>
          <img src={this.props.obj.url} alt=""></img>
          </td>
          <td>
            {this.props.obj.name}
          </td>
          <td>
            {this.props.obj.price}
          </td>
          <td>
            <Link to={"/edit/"+this.props.obj.id} className="btn btn-primary">Edit</Link>
          </td>
          <td>
            <button onClick={this.delete} className="btn btn-danger">Delete</button>
          </td>
        </tr>
    );
  }
}

export default TableRow;